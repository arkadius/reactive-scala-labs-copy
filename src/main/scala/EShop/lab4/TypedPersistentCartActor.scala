package EShop.lab4

import EShop.lab2.{Cart, TypedCheckout}
import EShop.lab3.TypedOrderManager
import akka.actor.Cancellable
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.{Effect, EventSourcedBehavior}

import scala.concurrent.duration._
import scala.util.Random

class TypedPersistentCartActor {

  import EShop.lab2.TypedCartActor._

  val cartTimerDuration: FiniteDuration = 5.seconds

  private def scheduleTimer(context: ActorContext[Command]): Cancellable = context.system.scheduler.scheduleOnce(cartTimerDuration, { () =>
    context.self ! ExpireCart
  })(context.executionContext)


  def apply(persistenceId: PersistenceId): Behavior[Command] = Behaviors.setup { context =>
    EventSourcedBehavior[Command, Event, State](
      persistenceId,
      Empty,
      commandHandler(context),
      eventHandler(context)
    )
  }

  def commandHandler(context: ActorContext[Command]): (State, Command) => Effect[Event, State] = (state, command) => {
    state match {
      case Empty =>
        command match {
          case AddItem(item) => Effect.persist(ItemAdded(item))
          case RemoveItem(item) => Effect.none
          case GetItems(sender) =>
            sender ! state.cart
            Effect.none
          case StartCheckout(_) => Effect.none
        }

      case NonEmpty(cart, _) =>
        command match {
          case AddItem(item) => Effect.persist(ItemAdded(item))
          case RemoveItem(item) =>
            if(state.cart.contains(item))
              Effect.persist(if(state.cart.size == 1) CartEmptied else ItemRemoved(item))
            else
              Effect.none
          case GetItems(sender) =>
            sender ! state.cart
            Effect.none
          case StartCheckout(orderManagerRef) =>
            val checkoutActor = context.spawn(new TypedPersistentCheckout().apply(context.self, PersistenceId.ofUniqueId("checkout")), "checkout")
            checkoutActor ! TypedCheckout.StartCheckout
            orderManagerRef ! TypedOrderManager.ConfirmCheckoutStarted(checkoutActor)
            Effect.persist(CheckoutStarted(checkoutActor))
          case ExpireCart => Effect.persist(CartExpired)
        }

      case InCheckout(_) =>
        command match {
          case AddItem(item) => Effect.none
          case ConfirmCheckoutClosed => Effect.persist(CheckoutClosed)
          case ConfirmCheckoutCancelled => Effect.persist(CheckoutCancelled)
          case CloseCheckout => Effect.persist(CheckoutClosed)
          case CancelCheckout => Effect.persist(CheckoutCancelled)
          case _ => Effect.none
        }
    }
  }

  def eventHandler(context: ActorContext[Command]): (State, Event) => State = (state, event) => {
    // ???
    event match {
      case CheckoutStarted(_)        => InCheckout(state.cart)
      case ItemAdded(item)           => NonEmpty(state.cart.addItem(item), scheduleTimer(context))
      case ItemRemoved(item)         => NonEmpty(state.cart.removeItem(item), scheduleTimer(context))
      case CartEmptied | CartExpired => Empty
      case CheckoutClosed            => Empty
      case CheckoutCancelled         => NonEmpty(state.cart, scheduleTimer(context))
    }
  }

}
