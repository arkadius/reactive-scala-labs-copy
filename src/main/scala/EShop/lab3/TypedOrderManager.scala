package EShop.lab3

import EShop.lab2.TypedCartActor.StartCheckout
import EShop.lab2.{TypedCartActor, TypedCheckout}
import EShop.lab4.TypedPersistentCartActor
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import akka.persistence.typed.PersistenceId

object TypedOrderManager {

  sealed trait Command
  case class AddItem(id: String, sender: ActorRef[Ack])                                               extends Command
  case class RemoveItem(id: String, sender: ActorRef[Ack])                                            extends Command
  case class SelectDeliveryAndPaymentMethod(delivery: String, payment: String, sender: ActorRef[Ack]) extends Command
  case class Buy(sender: ActorRef[Ack])                                                               extends Command
  case class Pay(sender: ActorRef[Ack])                                                               extends Command
  case class ConfirmCheckoutStarted(checkoutRef: ActorRef[TypedCheckout.Command])                     extends Command
  case class ConfirmPaymentStarted(paymentRef: ActorRef[TypedPayment.Command])                        extends Command
  case object ConfirmPaymentReceived                                                                  extends Command

  sealed trait Ack
  case object Done extends Ack //trivial ACK
}

class TypedOrderManager {

  import TypedOrderManager._

  def start: Behavior[TypedOrderManager.Command] = uninitialized

  def uninitialized: Behavior[TypedOrderManager.Command] = Behaviors.setup { ctx =>
    val cartActor = ctx.spawn(new TypedPersistentCartActor().apply(PersistenceId.ofUniqueId("cart")), "cart")
    open(cartActor)
  }

  def open(cartActor: ActorRef[TypedCartActor.Command]): Behavior[TypedOrderManager.Command] = Behaviors.receive {
    (ctx, msg) =>
      msg match {
        case AddItem(id, sender) =>
          cartActor ! TypedCartActor.AddItem(id)
          sender ! Done
          Behaviors.same
        case RemoveItem(id, sender) =>
          cartActor ! TypedCartActor.RemoveItem(id)
          sender ! Done
          Behaviors.same
        case Buy(sender) =>
          cartActor ! StartCheckout(ctx.self)
          inCheckout(cartActor, sender)
      }
  }

  def inCheckout(
    cartActorRef: ActorRef[TypedCartActor.Command],
    senderRef: ActorRef[Ack]
  ): Behavior[TypedOrderManager.Command] = Behaviors.receive {
    case (_, ConfirmCheckoutStarted(checkoutRef)) =>
      senderRef ! Done
      inCheckout(checkoutRef)
  }

  def inCheckout(checkoutActorRef: ActorRef[TypedCheckout.Command]): Behavior[TypedOrderManager.Command] =
    Behaviors.receive { (ctx, msg) =>
      msg match {
        case SelectDeliveryAndPaymentMethod(deliveryMethod, paymentMethod, sender) =>
          checkoutActorRef ! TypedCheckout.SelectDeliveryMethod(deliveryMethod)
          checkoutActorRef ! TypedCheckout.SelectPayment(paymentMethod, ctx.self)
          inPayment(sender)
      }
    }

  def inPayment(senderRef: ActorRef[Ack]): Behavior[TypedOrderManager.Command] = Behaviors.receive {
    case (_, ConfirmPaymentStarted(paymentRef)) =>
      senderRef ! Done
      inPayment(paymentRef, senderRef)
  }

  def inPayment(
    paymentActorRef: ActorRef[TypedPayment.Command],
    senderRef: ActorRef[Ack]
  ): Behavior[TypedOrderManager.Command] = Behaviors.receive { (ctx, msg) =>
    msg match {
      case Pay(sender) =>
        paymentActorRef ! TypedPayment.DoPayment
        sender ! Done
        Behaviors.same
      case ConfirmPaymentReceived => finished
    }
  }

  def finished: Behavior[TypedOrderManager.Command] = Behaviors.stopped
}
