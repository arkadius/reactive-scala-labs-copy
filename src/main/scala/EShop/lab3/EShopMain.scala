package EShop.lab3

import akka.actor.typed.{ActorRef, ActorSystem, Behavior}
import akka.actor.typed.scaladsl.Behaviors

import scala.io.StdIn.readLine
import akka.actor.typed.scaladsl.AskPattern._
import akka.util.Timeout

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Failure, Success}

object EShopMain {
  def apply(): Behavior[Any] = Behaviors.setup { (ctx) =>
    val orderManger = ctx.spawn(new TypedOrderManager().start, "order_manager")
    listening(orderManger)
  }

  def listening(orderManager: ActorRef[TypedOrderManager.Command]): Behavior[Any] = Behaviors.receive { (ctx, msg) =>
    msg match {
      case (sender: ActorRef[String], "add", x: String) =>
        orderManager ! TypedOrderManager.AddItem(x, ctx.self)
        waitingForResponse(orderManager, sender)
      case (sender: ActorRef[String], "remove", x: String) =>
        orderManager ! TypedOrderManager.RemoveItem(x, ctx.self)
        waitingForResponse(orderManager, sender)
      case (sender: ActorRef[String], "buy") =>
        orderManager ! TypedOrderManager.Buy(ctx.self)
        waitingForResponse(orderManager, sender)
      case (sender: ActorRef[String], "select", deliveryMethod: String, paymentMethod: String) =>
        orderManager ! TypedOrderManager.SelectDeliveryAndPaymentMethod(deliveryMethod, paymentMethod, ctx.self)
        waitingForResponse(orderManager, sender)
      case (sender: ActorRef[String], "pay") =>
        orderManager ! TypedOrderManager.Pay(ctx.self)
        waitingForResponse(orderManager, sender)
      case (sender: ActorRef[String], x: String) =>
        sender ! "unknown command"
        Behaviors.same
      case _ => Behaviors.same
    }
  }

  def waitingForResponse(orderManager: ActorRef[TypedOrderManager.Command], replyTo: ActorRef[String]): Behavior[Any] =
    Behaviors.receive { (ctx, msg) =>
      msg match {
        case TypedOrderManager.Done =>
          replyTo ! "ok"
          listening(orderManager)
      }
    }
}

object EShopApp extends App {
  println("Initializing eshop")
  val system = ActorSystem(EShopMain(), "eshop")
  println("Eshop initialized")
  implicit val timeout: Timeout = 2 seconds
  implicit val scheduler        = system.scheduler

  while (true) {
    println("Enter command: ")
    val command = readLine()
    val parts   = command.split(" ")
    val response: Future[String] = parts(0) match {
      case "add"    => system.ask(ref => (ref, parts(0), parts(1)))
      case "remove" => system.ask(ref => (ref, parts(0), parts(1)))
      case "buy"    => system.ask(ref => (ref, parts(0)))
      case "select" => system.ask(ref => (ref, parts(0), parts(1), parts(2)))
      case "pay"    => system.ask(ref => (ref, parts(0)))
      case _        => Future.failed(new IllegalArgumentException("Unknown command"))
    }

    if (!response.isCompleted)
      Await.result(response, 2 seconds)

    println(response.value match {
      case Some(Success(value)) => value
      case Some(Failure(value)) => "Fail:" + value.getMessage
      case None                 => "Not completed"
    })
  }
}
