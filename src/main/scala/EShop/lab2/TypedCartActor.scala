package EShop.lab2

import EShop.lab3.TypedOrderManager
import akka.actor.Cancellable
import akka.actor.typed.scaladsl.{Behaviors, TimerScheduler}
import EShop.lab3.TypedOrderManager
import akka.actor.Cancellable
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}

import scala.language.postfixOps
import scala.concurrent.duration._
import EShop.lab3.TypedOrderManager

object TypedCartActor {

  sealed trait Command
  case class AddItem(item: Any)                                                  extends Command
  case class RemoveItem(item: Any)                                               extends Command
  case object ExpireCart                                                         extends Command
  case class StartCheckout(orderManagerRef: ActorRef[TypedOrderManager.Command]) extends Command
  case object ConfirmCheckoutCancelled                                           extends Command
  case object ConfirmCheckoutClosed                                              extends Command
  case class GetItems(sender: ActorRef[Cart])                                    extends Command // command made to make testing easier
  case object CancelCheckout                                                     extends Command
  case object CloseCheckout                                                      extends Command

  case object ExpireCartTimerKey

  def apply() = new TypedCartActor().start

  sealed trait Event
  case class CheckoutStarted(checkoutRef: ActorRef[TypedCheckout.Command]) extends Event
  case class ItemAdded(item: Any)                                          extends Event
  case class ItemRemoved(item: Any)                                        extends Event
  case object CartEmptied                                                  extends Event
  case object CartExpired                                                  extends Event
  case object CheckoutClosed                                               extends Event
  case object CheckoutCancelled                                            extends Event

  sealed abstract class State(val timerOpt: Option[Cancellable]) {
    def cart: Cart
  }
  case object Empty extends State(None) {
    def cart: Cart = Cart.empty
  }
  case class NonEmpty(cart: Cart, timer: Cancellable) extends State(Some(timer))
  case class InCheckout(cart: Cart)                   extends State(None)
}

class TypedCartActor {

  import TypedCartActor._

  val cartTimerDuration: FiniteDuration = 5 seconds

  private def scheduleTimer(timers: TimerScheduler[Command]) = timers.startSingleTimer(ExpireCartTimerKey, ExpireCart, cartTimerDuration)

  def start: Behavior[TypedCartActor.Command] = empty

  def empty: Behavior[TypedCartActor.Command] = Behaviors.withTimers { timers =>
    Behaviors.receive { (ctx, cmd) =>
      cmd match {
        case AddItem(item) =>
          scheduleTimer(timers)
          nonEmpty(Cart.empty.addItem(item), null)
        case GetItems(sender) =>
          sender ! Cart.empty
          Behaviors.same
        }
      }
    }


  def nonEmpty(cart: Cart, timer: Cancellable): Behavior[TypedCartActor.Command] = Behaviors.withTimers { timers =>
    Behaviors.receive { (ctx, cmd) =>
      cmd match {
        case AddItem(item) =>
          scheduleTimer(timers)
          nonEmpty(cart.addItem(item), null)
        case RemoveItem(item) =>
          if (cart.contains(item)) {
            val newCart = cart.removeItem(item)
            if (newCart.size > 0) {
              scheduleTimer(timers)
              nonEmpty(newCart, null)
            } else
              empty
          } else {
            Behaviors.same
          }
        case StartCheckout(orderManagerRef) =>
          val checkoutActor = ctx.spawn(new TypedCheckout(ctx.self).start, "checkout")
          checkoutActor ! TypedCheckout.StartCheckout
          orderManagerRef ! TypedOrderManager.ConfirmCheckoutStarted(checkoutActor)
          inCheckout(cart)
        case ExpireCart =>
          empty
        case GetItems(sender) =>
          sender ! cart
          Behaviors.same
      }
    }
  }

  def inCheckout(cart: Cart): Behavior[TypedCartActor.Command] = Behaviors.withTimers { timers =>
    Behaviors.receive { (ctx, cmd) =>
      cmd match {
        case ConfirmCheckoutCancelled =>
          scheduleTimer(timers)
          nonEmpty(cart, null)
        case ConfirmCheckoutClosed =>
          empty
      }
    }
  }

}
