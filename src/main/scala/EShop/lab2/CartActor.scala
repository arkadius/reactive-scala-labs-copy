package EShop.lab2

import EShop.lab2.CartActor.ExpireCart
import akka.actor.{Actor, ActorRef, Cancellable, Props, Timers}
import akka.event.{Logging, LoggingReceive}

import scala.concurrent.duration._
import scala.language.postfixOps

object CartActor {

  sealed trait Command
  case class AddItem(item: Any)        extends Command
  case class RemoveItem(item: Any)     extends Command
  case object ExpireCart               extends Command
  case object StartCheckout            extends Command
  case object ConfirmCheckoutCancelled extends Command
  case object ConfirmCheckoutClosed    extends Command
  case object CancelCheckout extends Command
  case object CloseCheckout    extends Command
  case object GetItems                 extends Command // command made to make testing easier

  sealed trait Event
  case class CheckoutStarted(checkoutRef: ActorRef, cart: Cart) extends Event
  case class ItemAdded(itemId: Any, cart: Cart)                 extends Event
  case class ItemRemoved(itemId: Any, cart: Cart)               extends Event
  case object CartEmptied                                       extends Event
  case object CartExpired                                       extends Event
  case object CheckoutClosed                                    extends Event
  case class CheckoutCancelled(cart: Cart)                      extends Event

  case object ExpireCartTimerKey

  def props() = Props(new CartActor())
}

class CartActor extends Actor with Timers {

  import CartActor._

  private val log       = Logging(context.system, this)
  val cartTimerDuration = 5 seconds

  private def scheduleTimer = timers.startSingleTimer(ExpireCartTimerKey, ExpireCart, cartTimerDuration)

  def receive: Receive = empty

  def empty: Receive = {
    case AddItem(item) =>
      scheduleTimer
      context.become(nonEmpty(Cart.empty.addItem(item), null))
    case GetItems =>
      sender() ! Cart.empty
  }

  def nonEmpty(cart: Cart, timer: Cancellable): Receive = {
    case AddItem(item) =>
      scheduleTimer
      context.become(nonEmpty(cart.addItem(item), null))
    case RemoveItem(item) =>
      scheduleTimer
      if (cart.contains(item)) {
        val newCart = cart.removeItem(item)
        context.become(if (newCart.size > 0) nonEmpty(newCart, null) else empty)
      }
    case StartCheckout =>
      context.become(inCheckout(cart))
    case ExpireCart =>
      context.become(empty)
    case GetItems =>
      sender() ! cart
  }

  def inCheckout(cart: Cart): Receive = {
    case ConfirmCheckoutCancelled => context.become(nonEmpty(cart, null))
    case ConfirmCheckoutClosed    => context.become(empty)
  }

}
