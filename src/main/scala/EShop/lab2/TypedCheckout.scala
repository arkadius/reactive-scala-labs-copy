package EShop.lab2

import EShop.lab3.{TypedOrderManager, TypedPayment}
import akka.actor.Cancellable
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}

import scala.language.postfixOps
import scala.concurrent.duration._
import EShop.lab3.{TypedOrderManager, TypedPayment}

object TypedCheckout {

  sealed trait Command
  case object StartCheckout                                                                       extends Command
  case class SelectDeliveryMethod(method: String)                                                 extends Command
  case object CancelCheckout                                                                      extends Command
  case object ExpireCheckout                                                                      extends Command
  case class SelectPayment(payment: String, orderManagerRef: ActorRef[TypedOrderManager.Command]) extends Command
  case object ExpirePayment                                                                       extends Command
  case object ConfirmPaymentReceived                                                              extends Command
  case object ReceivePayment                                                                      extends Command

  sealed trait Event
  case object CheckOutClosed                                         extends Event
  case class PaymentStarted(payment: ActorRef[TypedPayment.Command]) extends Event
  case object CheckoutStarted                                        extends Event
  case object CheckoutCancelled                                      extends Event
  case class DeliveryMethodSelected(method: String)                  extends Event

  sealed abstract class State(val timerOpt: Option[Cancellable])
  case object WaitingForStart                           extends State(None)
  case class SelectingDelivery(timer: Cancellable)      extends State(Option(timer))
  case class SelectingPaymentMethod(timer: Cancellable) extends State(Option(timer))
  case object Closed                                    extends State(None)
  case object Cancelled                                 extends State(None)
  case class ProcessingPayment(timer: Cancellable)      extends State(Option(timer))
}

class TypedCheckout(
  cartActor: ActorRef[TypedCartActor.Command]
) {
  import TypedCheckout._

  val checkoutTimerDuration: FiniteDuration = 1 minute
  val paymentTimerDuration: FiniteDuration  = 1 minute

  private def scheduleCheckoutTimer(context: ActorContext[TypedCheckout.Command]): Cancellable =
    context.system.scheduler.scheduleOnce(checkoutTimerDuration, { () =>
      context.self ! ExpireCheckout
    })(context.executionContext)

  private def schedulePaymentTimer(context: ActorContext[TypedCheckout.Command]): Cancellable =
    context.system.scheduler.scheduleOnce(paymentTimerDuration, { () =>
      context.self ! ExpirePayment
    })(context.executionContext)

  def start: Behavior[TypedCheckout.Command] = Behaviors.receive { (ctx, cmd) =>
    cmd match {
      case StartCheckout =>
        selectingDelivery(scheduleCheckoutTimer(ctx))
    }
  }

  def selectingDelivery(timer: Cancellable): Behavior[TypedCheckout.Command] = Behaviors.receive { (ctx, cmd) =>
    cmd match {
      case SelectDeliveryMethod(method) =>
        timer.cancel()
        selectingPaymentMethod(scheduleCheckoutTimer(ctx))
      case ExpireCheckout =>
        cancelled
      case CancelCheckout =>
        cancelled
    }
  }

  def selectingPaymentMethod(timer: Cancellable): Behavior[TypedCheckout.Command] = Behaviors.receive { (ctx, cmd) =>
    cmd match {
      case SelectPayment(payment, orderManagerRef) =>
        timer.cancel()
        val paymentActor = ctx.spawn(new TypedPayment(payment, orderManagerRef, ctx.self).start, "payment")
        orderManagerRef ! TypedOrderManager.ConfirmPaymentStarted(paymentActor)
        processingPayment(schedulePaymentTimer(ctx))
      case ExpireCheckout =>
        cancelled
      case CancelCheckout =>
        cancelled
    }
  }

  def processingPayment(timer: Cancellable): Behavior[TypedCheckout.Command] = Behaviors.receive { (ctx, cmd) =>
    cmd match {
      case ConfirmPaymentReceived =>
        closed
      case ExpirePayment =>
        cancelled
      case CancelCheckout =>
        cancelled
    }
  }

  def cancelled: Behavior[TypedCheckout.Command] = Behaviors.setup { _ =>
    cartActor ! TypedCartActor.ConfirmCheckoutCancelled
    Behaviors.stopped
  }

  def closed: Behavior[TypedCheckout.Command] = Behaviors.setup { _ =>
    cartActor ! TypedCartActor.ConfirmCheckoutClosed
    Behaviors.stopped
  }

}
