package EShop.lab2

import EShop.lab2.Checkout._
import akka.actor.{Actor, ActorRef, Cancellable, Props, Timers}
import akka.event.{Logging, LoggingReceive}

import scala.concurrent.duration._
import scala.language.postfixOps

object Checkout {

  sealed trait Command
  case object StartCheckout                       extends Command
  case class SelectDeliveryMethod(method: String) extends Command
  case object CancelCheckout                      extends Command
  case object ExpireCheckout                      extends Command
  case class SelectPayment(payment: String)       extends Command
  case object ExpirePayment                       extends Command
  case object ConfirmPaymentReceived              extends Command
  case object ReceivePayment                      extends Command

  sealed trait Event
  case object CheckOutClosed                        extends Event
  case class PaymentStarted(payment: ActorRef)      extends Event
  case object CheckoutStarted                       extends Event
  case object CheckoutCancelled                     extends Event
  case class DeliveryMethodSelected(method: String) extends Event

  case object CheckoutTimerKey
  case object PaymentTimerKey

  def props(cart: ActorRef) = Props(new Checkout(cart))
}

class Checkout(
  cartActor: ActorRef
) extends Actor
  with Timers {

  private val scheduler = context.system.scheduler
  private val log       = Logging(context.system, this)

  val checkoutTimerDuration = 1 seconds
  val paymentTimerDuration  = 1 seconds

  private def scheduleCheckoutTimer = timers.startSingleTimer(CheckoutTimerKey, ExpireCheckout, checkoutTimerDuration)
  private def schedulePaymentTimer  = timers.startSingleTimer(PaymentTimerKey, ExpirePayment, paymentTimerDuration)

  def receive: Receive = {
    case StartCheckout =>
      scheduleCheckoutTimer
//      sender ! SelectingDeliveryStarted(null)
      context.become(selectingDelivery(null))
  }

  def selectingDelivery(timer: Cancellable): Receive = {
    case SelectDeliveryMethod(method) =>
      scheduleCheckoutTimer
//      sender ! PaymentStarted(self)
      context.become(selectingPaymentMethod(null))
    case ExpireCheckout =>
      context.become(cancelled)
    case CancelCheckout =>
      context.become(cancelled)

  }

  def selectingPaymentMethod(timer: Cancellable): Receive = {
    case SelectPayment(payment) =>
      schedulePaymentTimer
//      sender ! ProcessingPaymentStarted(null)
      context.become(processingPayment(null))
    case ExpireCheckout =>
      context.become(cancelled)
    case CancelCheckout =>
      context.become(cancelled)
  }

  def processingPayment(timer: Cancellable): Receive = {
    case ConfirmPaymentReceived =>
      context.become(closed)
    case ExpirePayment =>
      context.become(cancelled)
    case CancelCheckout =>
      context.become(cancelled)
  }

  def cancelled: Receive = {
    case _ => context.stop(self)
  }

  def closed: Receive = {
    case _ => context.stop(self)
  }

}
