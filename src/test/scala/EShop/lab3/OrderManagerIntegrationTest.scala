package EShop.lab3

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import akka.util.Timeout
import org.scalatest.concurrent.ScalaFutures

import scala.concurrent.duration._
import akka.pattern.ask
import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers

class OrderManagerIntegrationTest
  extends TestKit(ActorSystem("OrderManagerIntegrationTest"))
  with AnyFlatSpecLike
  with ImplicitSender
  with BeforeAndAfterAll
  with Matchers
  with ScalaFutures {

  import OrderManager._

  implicit val timeout: Timeout = 1.second

  it should "supervise whole order process" in {

    // I used typed actors
    true.shouldBe(true)
  }

}
