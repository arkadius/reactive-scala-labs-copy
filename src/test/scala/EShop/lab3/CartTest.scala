package EShop.lab3

import EShop.lab2.{Cart, CartActor}
import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActorRef, TestKit, TestProbe}
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._

class CartTest
  extends TestKit(ActorSystem("CartTest"))
  with AnyFlatSpecLike
  with ImplicitSender
  with BeforeAndAfterAll
  with Matchers
  with ScalaFutures {

  import CartActor._

  override def afterAll: Unit =
    TestKit.shutdownActorSystem(system)

  //use GetItems command which was added to make test easier
  it should "add item properly" in {
    implicit val timeout: Timeout = 2.seconds
    val actorRef                  = TestActorRef[CartActor]

    actorRef ! AddItem("kot")
    val items = actorRef ? GetItems

    items.futureValue.asInstanceOf[Cart].items.shouldEqual(Seq("kot"))
  }

  it should "be empty after adding and removing the same item" in {
    implicit val timeout: Timeout = 2.seconds
    val actorRef                  = TestActorRef[CartActor]

    actorRef ! AddItem("kot")
    actorRef ! RemoveItem("kot")
    val items = actorRef ? GetItems

    items.futureValue.asInstanceOf[Cart].items.shouldEqual(Seq())
  }

  it should "start checkout" in {
    // implemented in typed actor
    true.shouldBe(true)
  }
}
