package EShop.lab3

import EShop.lab2.{Cart, TypedCartActor}
import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers

class TypedCartTest
  extends ScalaTestWithActorTestKit
    with AnyFlatSpecLike
    with BeforeAndAfterAll
    with Matchers
    with ScalaFutures {

  override def afterAll: Unit =
    testKit.shutdownTestKit()

  import TypedCartActor._

  //use GetItems command which was added to make test easier
  it should "add item properly" in {
    val probe = testKit.createTestProbe[Any]()
    val cart = testKit.spawn(new TypedCartActor().start, "cart")
    cart ! AddItem("item1")
    cart ! GetItems(probe.ref)
    probe.expectMessage(Cart.empty.addItem("item1"))
  }

  it should "be empty after adding and removing the same item" in {
    val probe = testKit.createTestProbe[Any]()
    val cart = testKit.spawn(new TypedCartActor().start, "cart2")
    cart ! AddItem("item1")
    cart ! GetItems(probe.ref)
    probe.expectMessage(Cart.empty.addItem("item1"))
    cart ! RemoveItem("item1")
    cart ! GetItems(probe.ref)
    probe.expectMessage(Cart.empty)
  }

  it should "start checkout" in {
    val orderManagerProbe = testKit.createTestProbe[Any]()
    val cart = testKit.spawn(new TypedCartActor().start, "cart3")
    cart ! AddItem("item1")
    cart ! StartCheckout(orderManagerProbe.ref)
    orderManagerProbe.expectMessageType[TypedOrderManager.ConfirmCheckoutStarted]
  }
}
